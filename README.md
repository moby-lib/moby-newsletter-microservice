# Moby Newsletter/Notification Microservice

The Newsletter/Notification microservice for the Moby Lib project.

## REST API

|Method|Path|Description|Req Body|Res Body|Minimum Role|
|---|---|---|---|---|---|
| GET | /api/notification/newsletter/subscribe | Subscribes this user. |  | {"subscriptionId": <subscription_id>} | READER |
| DELETE | /api/notification/newsletter/unsubscribe | Unsubscribes this user. |  |  | READER |
| DELETE | /api/notification/newsletter/user/:userId | **INTERNAL**. Unsubscribe the user with given userid. |  |  | None |
| POST | /api/notification/newsletter/notify | **INTERNAL**. Used by other microservice to notify that a new book was added in the library. It will create a request to the mailing microservice to email all the subscribers of the newly added book. | {"bookName": <book_name>, "author": <author_name>} |  | None |

## Deployment

### Monorepo configurations - RECOMMENDED

[monorepo](https://gitlab.com/moby-lib/moby-monorepo)

### Using Docker - NOT RECOMMENDED
```
sudo docker network create newsletter-net
sudo docker run --name moby-newsletter-microservice --network="newsletter-net" --env-file .env -p 3002:3002 registry.gitlab.com/moby-lib/moby-newsletter-microservice:1.0
sudo docker network connect microservices-net moby-newsletter-microservice

cd database-deploy; sudo docker-compose up
sudo docker network connect newsletter-net database-deploy_newsletter-db_1
```