const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const {
    MailingPayload
} = require('./models.js');

const { 
    sendRequest
} = require('../http-client');

const subscribe = async (userId, email) => {
    console.info(`Subscribing user ${userId} with email ${email}.`);

    try {
        const id = await query('INSERT INTO newsletter (id_user, to_email) VALUES ($1, $2) RETURNING id', [userId, email]);
        return id;
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Already subscribed!', 409);
        }
        throw e;
    }
};

const unsubscribe = async (id) => {
    console.info(`Unsubscribing user ${id}.`);

    await query('DELETE FROM newsletter WHERE id_user = $1', [id]);
};

const notify = async (bookName, author) => {
    console.info(`Notifying users for the new book ${bookName} by ${author}.`);

    const emails = await query('SELECT id_user, to_email FROM newsletter');

    payload = new MailingPayload(bookName, author, emails);

    const options = {
        url: `http://${process.env.MAILING_SERVICE}/api/newsletter`,
        method: 'POST',
        data: payload
    };

    await sendRequest(options);
};

module.exports = {
    subscribe,
    unsubscribe,
    notify
}