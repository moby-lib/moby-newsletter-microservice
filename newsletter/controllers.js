const Router = require('express').Router();

const {
    ServerError
} = require('../errors');

const {
    subscribe,
    unsubscribe,
    notify
} = require('./services.js');

const {
    authorizeUser
} = require('../authorization');

const {
    ADMIN_ROLE,
    READER_ROLE
} = process.env

Router.get('/subscribe', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId,
        email
    } = req.state;

    const subscriptionId = await subscribe(userId, email);

    res.json({subscriptionId});
});

Router.delete('/unsubscribe', authorizeUser(ADMIN_ROLE, READER_ROLE), async (req, res) => {
    const {
        userId
    } = req.state;

    await unsubscribe(userId);

    res.status(204).end();
});

/**
 * Internal route used for sync when a user is deleted.
 */
Router.delete('/unsubscribe/user/:userId', async (req, res) => {
    const {
        userId
    } = req.params

    await unsubscribe(userId);

    res.status(204).end();
});

/**
 * Internal route used by the library ms when a new book is added.
 */
Router.post('/notify', async (req, res) => {
    const {
        bookName,
        author
    } = req.body;

    await notify(bookName, author);

    res.status(200).end();
});

module.exports = Router;