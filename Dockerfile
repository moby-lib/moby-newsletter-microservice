FROM node:14-alpine

WORKDIR /usr/src/app
COPY package*.* ./
RUN npm ci
COPY . .

EXPOSE 3002

CMD ["npm", "run", "start"]
