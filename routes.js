const Router = require('express').Router();

const newsletterControllers = require('./newsletter/controllers.js');

Router.use('/newsletter', newsletterControllers);

module.exports = Router;